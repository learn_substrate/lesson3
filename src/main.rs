// practive
// fn print_number(maybe_number: Option<u16>) {
//     println!("printing: {}", maybe_number.unwrap());
// }

// fn main() {
//     print_number(Some(13));
//     print_number(Some(99));

//     let mut numbers: [Option<u16>; 5] = [None, None, None, None, None];
//     for iter in 0..5 {
//         let number_to_add: u16 = { ((iter * 1235) + 2) / (4 * 16) };

//         numbers[iter as usize] = Some(number_to_add);
//     }
// }

// practive
// #[derive(Debug, Clone)]
// struct MyData {
//     val1: i32,
//     val2: String,
// }

// fn main() {
//     let d = MyData {
//         val1: 35,
//         val2: String::from("Hello World"),
//     };

//     let both = d.get_both();
//     let x = d.get_val1();
//     let y = d.get_val2();
// }
// impl MyData {
//     pub fn get_val1(&self) -> i32 {
//         return self.val1.clone();
//     }

//     pub fn get_val2(&self) -> String {
//         return self.val2.clone();
//     }

//     pub fn get_both(&self) -> (i32, String) {
//         return (self.val1, self.val2.to_string());
//     }
// }

// practive
// fn main() {
//     let a = A {
//         p: Some("p".to_string()),
//     };
//     a.a();
// }

// struct A {
//     p: Option<String>,
// }

// impl A {
//     fn a(self) -> Self {
//         Self::b(&self.p.as_ref().unwrap());
//         self
//     }
//     fn b(b: &str) {
//         print!("b: {}", b)
//     }
// }

// *** === Exercise === ***

use itertools::Itertools;
use std::collections::HashMap;

pub struct School {
    students: HashMap<String, u32>,
}

impl School {
    pub fn new() -> School {
        School {
            students: HashMap::new(),
        }
    }
    //1. Có thể thêm thông tin của sinh viên gồm có tên và điểm
    pub fn add(&mut self, grade: u32, student: &str) {
        self.students.insert(student.to_string(), grade);
    }
    //2. Liệt kê các điểm số hiện tại mà trường đã cập nhập
    pub fn grades(&self) -> Vec<u32> {
        let mut arr_val = Vec::new();
        for (key, val) in self.students.iter() {
            arr_val.push(*val);
        }
        arr_val.sort();
        arr_val.dedup();
        arr_val
    }
    //3. Liệt kê danh sách các học sinh có cùng 1 điểm số
    pub fn grade(&self, grade: u32) -> Vec<String> {
        let mut arr_val = Vec::new();
        let mut _val: u32 = 0;
        for (key, val) in self.students.iter() {
            if (grade == *val) {
                arr_val.push(key.to_string());
            }
        }
        arr_val.sort();
        arr_val
    }

    pub fn get_all_Students(&self) {
        println!("get_all_Students {:?}", self.students);
    }
}

fn main() {
    let mut school = School::new();
    school.get_all_Students();
    school.add(1, "alice");
    school.add(2, "bob");
    school.add(2, "akle");
    let _grades = school.grades();
    println!("grades {:?}", _grades);

    let find_student = school.grade(2);
    println!("grades {:?}", find_student);
    school.get_all_Students();
}
